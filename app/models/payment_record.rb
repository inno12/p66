class PaymentRecord < ApplicationRecord
  belongs_to :user

  require 'csv'

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |payment_record|
        csv << payment_record.attributes.values
      end
    end
  end

end
