class PayoutRecord < ApplicationRecord
  require 'csv'

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |payout_record|
        csv << payout_record.attributes.values
      end
    end
  end

end
