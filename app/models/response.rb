class Response < ApplicationRecord
  belongs_to :test_record, required: false
  mount_uploader :avatar, AudioUploader
end
