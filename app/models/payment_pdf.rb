require "render_anywhere"

class PaymentPdf
  include RenderAnywhere

  def initialize(current_user, record_id)
    @user = current_user
    @payment_records = PaymentRecord.where(:user_id => @user.id)
  end

  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/public/payment.pdf")
  end

  private

    def as_html
      render template: "payment_records/download_pdf", locals: { user: @user, payment_records: @payment_records }
    end
end
