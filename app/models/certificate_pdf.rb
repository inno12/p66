require "render_anywhere"

class CertificatePdf
  include RenderAnywhere

  def initialize(current_user, record_id)
    @user = current_user
    @test_record = TestRecord.where(:user_id => @user.id, :id => record_id).first
  end

  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/public/certificate.pdf")
  end

  private

    def as_html
      render template: "users/certificate", layout: "certificate_pdf", locals: { user: @user, record: @test_record }
    end
end
