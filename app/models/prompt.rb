class Prompt < ApplicationRecord
  # has_many :responses
  mount_uploader :video, VideoUploader
  mount_uploader :audio, AudioUploader
  mount_uploader :image, AvatarUploader

  def self.fix_santax
    # text = text.gsub("\r\n","\\n").gsub("\r","").gsub("\n","\\n");
    text = "test"
  end

  def self.find_by_prompt_id prompt_id
    prompt = Prompt.find_by_id(prompt_id)
    prompt.text = prompt.text.gsub("\r\n","").gsub("'","&#39;") if !prompt.text.nil?
    return prompt
  end
end
