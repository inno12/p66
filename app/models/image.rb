class Image < ApplicationRecord
  mount_uploader :file, AvatarUploader
end
