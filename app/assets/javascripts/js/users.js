// change user Status
$('.btn-on-off').click(function(){
  var user_status = 0;
  if ($(this).is(':checked')) {
    user_status = 1;
  }else {
    user_status = 0;
  }
  var user_id = $(this).val();
  // console.log('user_id =' + user_id);
  // console.log('user_status = ' + user_status);
  $.ajax({
    url: '/change_user_status',
    type: 'post',
    data: {
      user_id: user_id,
      user_status : user_status
    },
    // success: function() {console.log(data);},
    // error: function() {console.log('error');}
  });

});

// invite users
$('#send_invitation').click(function(){
  var invite_email = $('#invite_email').val();
  var invite_subject = $('#invite_subject').val();
  var invite_content = $('#invite_content').val();
  console.log(invite_email);
  console.log(invite_subject);
  console.log(invite_subject);

  $.post( "/invite_user", {
    invite_email: invite_email,
    invite_subject: invite_subject,
    invite_content: invite_content
  });
});

// invite as rater
$('#invite_as_rater').click(function(){
  var id = this.value;
  // console.log(id);
  $.post( "/invite_as_rater", {
    user_id: id
  });
});
