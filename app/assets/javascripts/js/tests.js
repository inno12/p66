$("li.admin-tests").addClass("active");
$("#create-tests").addClass("active");

// Activate Next Step

$(document).ready(function() {
    // DEMO ONLY //
    $('#activate-step-1').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#test-ilr1"]').trigger('click');
        // $(this).remove();
    })

    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#test-ilr1-plus"]').trigger('click');
        // $(this).remove();
    })
    $('#activate-step-3').on('click', function(e) {
        $('ul.setup-panel li:eq(3)').removeClass('disabled');
        $('ul.setup-panel li a[href="#test-ilr2"]').trigger('click');
        // $(this).remove();
    })


    $('#activate-step-4').on('click', function(e) {
        $('ul.setup-panel li:eq(4)').removeClass('disabled');
        $('ul.setup-panel li a[href="#test-ilr2-plus"]').trigger('click');
        // $(this).remove();
    })

    $('#activate-step-5').on('click', function(e) {
        $('ul.setup-panel li:eq(5)').removeClass('disabled');
        $('ul.setup-panel li a[href="#test-ilr3"]').trigger('click');
        // $(this).remove();
    })

});


// test status
$('#test_status').click(function() {
  if ($(this).is(':checked')) {
    $(this).val("1");
  }else {
    $(this).val("0");
  }
});

$('#is_practice').click(function() {
  if ($(this).is(':checked')) {
    $(this).val("1");
  }else {
    $(this).val("0");
  }
});


// prevent selecting the same option from a different select box
$(".select-prompt").change(function(){
  $(".select-prompt option").prop("disabled",""); //enable everything
 //collect the values from selected;
  var arr = $.map($(".select-prompt option:selected"), function(n){
    return n.value;
  });
  $(".select-prompt option").filter(function(){
    return $.inArray($(this).val(),arr)>-1;
  }).prop("disabled","disabled");
});

  // create test
$('#create_test').click(function(){
  var test_status = $('#test_status').val();
  var is_practice = $('#is_practice').val();
  var test_title = $('#test_title').val();
  var test_language = $('#test_language').val();
  var test_industry = $("#test_industry").val();
  var test_description = $('#test_description').val();
  var test_time = $('#test_time').val();
  var test_price = $('#test_price').val();

  var test_ilr1_prompt1 = $('#ilr1_prompt1 option:selected').val();
  var test_ilr1_prompt2 = $('#ilr1_prompt2 option:selected').val();
  var test_ilr1plus_prompt1 = $('#ilr1plus_prompt1 option:selected').val();
  var test_ilr1plus_prompt2 = $('#ilr1plus_prompt2 option:selected').val();
  var test_ilr2_prompt1 = $('#ilr2_prompt1 option:selected').val();
  var test_ilr2_prompt2 = $('#ilr2_prompt2 option:selected').val();
  var test_ilr2plus_prompt1 = $('#ilr2plus_prompt1 option:selected').val();
  var test_ilr2plus_prompt2 = $('#ilr2plus_prompt2 option:selected').val();
  var test_ilr3_prompt1 = $('#ilr3_prompt1 option:selected').val();
  var test_ilr3_prompt2 = $('#ilr3_prompt2 option:selected').val();

  $.post("/tests", {
    status: test_status,
    is_practice: is_practice,
    title: test_title,
    language: test_language,
    industry: test_industry,
    description: test_description,
    time: test_time,
    price: test_price,
    ilr1_prompt1: test_ilr1_prompt1,
    ilr1_prompt2: test_ilr1_prompt2,
    ilr1plus_prompt1: test_ilr1plus_prompt1,
    ilr1plus_prompt2: test_ilr1plus_prompt2,
    ilr2_prompt1: test_ilr2_prompt1,
    ilr2_prompt2: test_ilr2_prompt2,
    ilr2plus_prompt1: test_ilr2plus_prompt1,
    ilr2plus_prompt2: test_ilr2plus_prompt2,
    ilr3_prompt1: test_ilr3_prompt1,
    ilr3_prompt2: test_ilr3_prompt2
  }, function (data, status) {
    if (data.status == "false") {
    // console.log("Data: " + data.status);
    //   console.log("Data: " + data.message);
      return;
    } else {
      // console.log("Data: " + data.status);
      // console.log("Data: " + data.message);
    }
  });

});

// update test
$('#update_test').click(function(){
  var test_status = $('#test_status').val();
  var is_practice = $('#is_practice').val();
  var test_title = $('#test_title').val();
  var test_language = $('#test_language').val();
  var test_industry = $("#test_industry").val();
  var test_description = $('#test_description').val();
  var test_time = $('#test_time').val();
  var test_price = $('#test_price').val();

  var test_ilr1_prompt1 = $('#ilr1_prompt1 option:selected').val();
  var test_ilr1_prompt2 = $('#ilr1_prompt2 option:selected').val();
  var test_ilr1plus_prompt1 = $('#ilr1plus_prompt1 option:selected').val();
  var test_ilr1plus_prompt2 = $('#ilr1plus_prompt2 option:selected').val();
  var test_ilr2_prompt1 = $('#ilr2_prompt1 option:selected').val();
  var test_ilr2_prompt2 = $('#ilr2_prompt2 option:selected').val();
  var test_ilr2plus_prompt1 = $('#ilr2plus_prompt1 option:selected').val();
  var test_ilr2plus_prompt2 = $('#ilr2plus_prompt2 option:selected').val();
  var test_ilr3_prompt1 = $('#ilr3_prompt1 option:selected').val();
  var test_ilr3_prompt2 = $('#ilr3_prompt2 option:selected').val();

  $.ajax({
    type: 'PUT',
    url: $(this).attr('ajax_path'),
    dataType: "json",
    data: { test: {
        status: test_status,
        is_practice: is_practice,
        title: test_title,
        language: test_language,
        industry: test_industry,
        description: test_description,
        time: test_time,
        price: test_price,
        ilr1_prompt1: test_ilr1_prompt1,
        ilr1_prompt2: test_ilr1_prompt2,
        ilr1plus_prompt1: test_ilr1plus_prompt1,
        ilr1plus_prompt2: test_ilr1plus_prompt2,
        ilr2_prompt1: test_ilr2_prompt1,
        ilr2_prompt2: test_ilr2_prompt2,
        ilr2plus_prompt1: test_ilr2plus_prompt1,
        ilr2plus_prompt2: test_ilr2plus_prompt2,
        ilr3_prompt1: test_ilr3_prompt1,
        ilr3_prompt2: test_ilr3_prompt2
      }
    },
  success: function(){
    location.href = '/tests';
  }

  });

});

// browser audio recorder

  var audio_context;
  var recorder;
  var isRecording = false;
  var recordingsList;
  var gumStream; //stream from getUserMedia()
  var input;
  function startUserMedia(stream) {
    console.log("getUserMedia() success, stream created, initializing Recorder.js ...");
    /* assign to gumStream for later use */
    gumStream = stream;

    input = audio_context.createMediaStreamSource(stream);
    // Uncomment if you want the audio to feedback directly
    //input.connect(audio_context.destination);

    recorder = new Recorder(input);
    console.log("Recording started");
    $(".active-slide .btn-record").removeAttr("disabled");
  }
  var promptRef = 0;
  var audiocomplementary = 0;
  var videocomplementary = 0;

  function recorder_disable() {
    $("#recorder_btn_" + (promptRef+1)).attr ("disabled", "disabled");
  }

  function recorder_able() {

    var testAudio = $(".test-audio")[promptRef];
    var testVideo = $(".test-video")[promptRef];
    if (($(testAudio).find("audio").length > 0) && ($(testVideo).find("video").length > 0)){

      if (($("audio")[promptRef+audiocomplementary].paused == true) && ($("video")[promptRef+videocomplementary].paused == true)){
        $("#recorder_btn_"+ (promptRef+1)).removeAttr("disabled");
      }
    }
    else {
      $("#recorder_btn_"+ (promptRef+1)).removeAttr("disabled");
    }
  }

  function startRecording(button) {
    console.log("recordButton clicked");
    isRecording = true;
    recorder && recorder.record();
    countDown(button);
    getRecordingsList(button);
    // setTimeout(function() {
    //   recorder.stop();
    // }, 5000);
    // button.disabled = true;
    // sendWaveToPost();
    // recorder.clear();
  }

  function stopRecording() {
    console.log("stopButton clicked");
    isRecording = false;
    recorder && recorder.stop();
    $('#next').attr('disabled', false);
    // recordingslist.innerHTML = "";
    sendWaveToPost();
    recorder.clear();
  }

  function countDown(button){
    $this = $(button).parent().find('.time-limit');
    var original_time_limit = parseInt($this.html());
    var time_limit = original_time_limit;

    // if(time_limit > 0 && isRecording)
    // {
    //   setTimeout(function() {
    //     time_limit --;
    //     $this.html(time_limit);
    //     countDown(button)
    //   }, 1000);
    // } else if (isRecording) {
    //   $(button).parent().find('.response-stop-record').trigger('click');
    // }

    var timer = setInterval(function() {
      if(time_limit > 0 && isRecording)
      {
        time_limit --;
        $this.html(time_limit);
        // console.log('timer run run run');
      } else if (isRecording) {
        $(button).parent().find('.response-stop-record').trigger('click');
        // console.log('timer finished');
        clearInterval(timer)
        $this.html(original_time_limit);
      }
      else {
        // console.log('timer stopped');
        clearInterval(timer)
        $this.html(original_time_limit);
      }
    }, 1000);
  }

  var recordingsList;
  function getRecordingsList(button) {
    recordingsList = $(button).parent().find('#recordingslist');
  }

  var data = new FormData();
  // var url;
  // var name;
  function sendWaveToPost() {
    recorder && recorder.exportWAV(function(blob) {
      var url = URL.createObjectURL(blob);
      var name = new Date().toISOString() + '.wav';
      var audioHTML = '<audio controls controlslist="nodownload"><source src="' + url + '" type="audio/wav"></audio>';
   
      //add the li element to the ordered list
      recordingsList.html(audioHTML);
      
      data.set('avatar', blob, name);
      // Display the key/value pairs
      for (var pair of data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]+ ', ' + pair[2]); 
      }
    });
  }
  window.onload = function init() {
    try {
      // webkit shim
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);
      window.URL = window.URL || window.webkitURL;

      audio_context = new AudioContext;
    } catch (e) {
      alert('No web audio support in this browser!');
    }

    navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
      console.log("getUserMedia failed : " + e);
    });
  };
