
$('.modal').on('show.bs.modal', function () {

  $('.modal').not($(this)).each(function () {
    $(this).modal('hide');
  });
});

$(function(){
  var url = new URL(location.href);
  var status = url.searchParams.get("status");

  if(status == 'login' && $('#login-btn').length > 0){
    $('#login-btn').trigger('click');
  } else if (status == 'changedpassword' && $('#login-btn').length > 0){
    $('#login-btn').trigger('click');
    $('#login_error_msg').html('Your password was successfully changed.');
  }
});


// forget password
$('#InputEmailForgotPass').keypress(function(e){
  if(e.which === 13){
      $("#forgot-pass-submit").trigger('click');
      return false;  // Prevent the default button behaviour

  }
});
$("#forgot-pass-submit").click(function () {


  $("#forget_password_error_msg").empty();

  var email = $('#InputEmailForgotPass').val();

  if(email == ""){
      $("#forget_password_error_msg").append( "<p style='color:red;'>Please Type in Email</p>" );
      return;
  }

  $.post("/forget_password", {
    email: email
  }, function (data, status) {
    if(data.status == "false"){
      $( "#forget_password_error_msg" ).append( "<p style='color:red;'>"+data.message+"</p>" );
      return;
    }else {
      // console.log("Data: " + data + "\nStatus: " + status);
      $(this).attr("data-dismiss","modal");

      console.log($(this)[0]);
      console.log("Data: " + data.status);
      console.log("Data: " + data.message);
      $('#modal-forgot-pass').modal('hide');
      $('#modal-thankyou-password').modal('show');

    }
  });
});

// forget email
$('.forget_email_input').keypress(function(e){
  if(e.which === 13){
      $("#forgot-email-submit").trigger('click');
      return false;  // Prevent the default button behaviour

  }
});
$("#forgot-email-submit").click(function () {

  $( "#forget_email_error_msg" ).empty();

  var f_name = $('#InputFirstname').val();
  var l_name = $('#InputLastname').val();
  var doy = $('#user_date_of_birth_1i').val();
  var dom = $('#user_date_of_birth_2i').val();
  var dod = $('#user_date_of_birth_3i').val();
  var date_of_birth = doy + "-" + dom + "-" + dod;

  if(f_name == ""){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>Please Type in First Name</p>" );
      return;
  }
  if(l_name == ""){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>Please Type in Last Name</p>" );
      return;
  }
  if(doy == ""){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>Please Select Year of Birth</p>" );
      return;
  }
  if(dom == ""){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>Please Select Month of Birth</p>" );
      return;
  }

  if(dod == ""){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>Please Select Day of Birth</p>" );
      return;
  }

  $.post("/forget_email", {
    f_name: f_name,
    l_name: l_name,
    date_of_birth: date_of_birth
  }, function (data, status) {
    if(data.status == "false"){
      $( "#forget_email_error_msg" ).append( "<p style='color:red;'>"+data.message+"</p>" );

      return;
    }else{
      // console.log("Data: " + data + "\nStatus: " + status);
      console.log("Data: " + data.status);
      console.log("Data: " + data.message);
      $('#forgot-email-submit').attr("data-dismiss","modal");
      $('#modal-forgot-email').modal('hide');
      $('#modal-thankyou-email').modal('show');
    }
  });
});

// login
$(".login").keypress(function(e){
    if(e.which === 13){
        $("#login_submit").trigger('click');
        return false;  // Prevent the default button behaviour

    }
});

$("#login_submit").click(function () {

  $( "#login_error_msg" ).empty();

  var email = $('#InputEmail1').val();
  var password = $('#InputPassword1').val();

  if(email == ""){
      $( "#login_error_msg" ).append( "<p style='color:red;'>Please Type in Email</p>" );

      return;
  }

  if(password == ""){
      $( "#login_error_msg" ).append( "<p style='color:red;'>Please Type in Password</p>" );

      return;
  }


  $.post("/login", {
    email: email,
    password: password
  }, function (data, status) {
    // console.log("Data: " + data + "\nStatus: " + status);
    if(data.status == "false"){
      $( "#login_error_msg" ).append( "<p style='color:red;'>"+data.message+"</p>" );
      return;
    }else{
      //redirect to cms
      // console.log("Data: " + data.status);
      // console.log("Data: " + data.message);
      // window.location.href = "/home";
    }
  });
});


// validiation
$(document).ready(function() {

    FormValidation.Validator.securePassword = {
      validate: function(validator, $field, options) {
          var value = $field.val();
          if (value === '') {
              return true;
          }
          return true;
      }
  };

    $('#login').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'login_password': {
              trigger: 'blur',
              validators: {
                notEmpty: {
                  message: 'The password is required and cannot be empty'
                },
                securePassword: {
                  message: 'The password is not valid'
                }
              }
            },
            'login_email': {
              trigger: 'blur',
              validators: {
                notEmpty: {
                  message: 'The email is required and cannot be empty'
                },
                regexp: {
                  regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                  message: 'The value is not a valid email address'
                }
              }
           }
        }
    }).on('err.validator.fv', function(e, data) {
          // $(e.target)    --> The field element
          // data.fv        --> The FormValidation instance
          // data.field     --> The field name
          // data.element   --> The field element
          // data.validator --> The current validator name
          data.element
              .data('fv.messages')
              // Hide all the messages
              .find('.help-block[data-fv-for="' + data.field + '"]').hide()
              // Show only message associated with current validator
              .filter('[data-fv-validator="' + data.validator + '"]').show();
    });

      $('#forget_email').formValidation({
          framework: 'bootstrap',
          icon: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            'forget_f_name': {
              validators: {
                notEmpty: {
                    message: 'The field is required and cannot be empty'
                }
              }
           },
              'forget_l_name': {
                validators: {
                  notEmpty: {
                      message: 'The field is required and cannot be empty'
                  }
                }
             }
          }
      }).on('err.validator.fv', function(e, data) {
              // $(e.target)    --> The field element
              // data.fv        --> The FormValidation instance
              // data.field     --> The field name
              // data.element   --> The field element
              // data.validator --> The current validator name
              data.element
                  .data('fv.messages')
                  // Hide all the messages
                  .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                  // Show only message associated with current validator
                  .filter('[data-fv-validator="' + data.validator + '"]').show();
          });
          $('#forget_password').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                'forget_email': {
                  trigger: 'blur', //updated by Johann, 2018-06-07
                  validators: {
                    notEmpty: {
                        message: 'The field is required and cannot be empty'
                    },
                      regexp: {
                          regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                          message: 'The value is not a valid email address'
                      }
                  }
               }
              }
          }).on('err.validator.fv', function(e, data) {
                  // $(e.target)    --> The field element
                  // data.fv        --> The FormValidation instance
                  // data.field     --> The field name
                  // data.element   --> The field element
                  // data.validator --> The current validator name
                  data.element
                      .data('fv.messages')
                      // Hide all the messages
                      .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                      // Show only message associated with current validator
                      .filter('[data-fv-validator="' + data.validator + '"]').show();
              });
});
