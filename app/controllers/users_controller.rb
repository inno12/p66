class UsersController < ApplicationController
  # before_action :set_user, only: [:show, :edit, :update, :destroy]
  # before_action :authuser, only: [:home, :edit, :update, :prompts, :sendscores, :dashboard]
  before_action :authuser, except: [:new, :create, :login, :logout]
  before_action :footer
  # GET /users
  # GET /users.json

  require 'date'

  def footer
    @footer = Page.find_by_page_type("footer")
  end

  def login

      @email =  params[:email]
      @password = params[:password]

      @current_user = User.find_by_email(@email)

      if @current_user == nil
        data = {:message => "Couldn't find your Parrot66 account.", :status => "false"}
        return render :json => data, :status => :ok
      end

      @current_user_password = @current_user["password"]
      @id = @current_user["id"]

      @encrypted_password = BCrypt::Password.new(@current_user_password)
      @auth = (@encrypted_password == @password)

      if @auth != true
        data = {:message => "The password entered does not match the password for this account. Please try again.", :status => "false"}
        return render :json => data, :status => :ok
      end


      @current_user_confirm_id = @current_user["id"]
      @current_user_status = @current_user["status"]

      # if @current_user_status.to_s != "1"
      #   data = {:message => "Your Account is Disable, Please contact your admin to enable it.", :status => "false"}
      #   return render :json => data, :status => :ok
      # end

      # data = {:message => @current_user_status, :status => "false"}
      # return render :json => data, :status => :ok

      # if @current_user_status == "0"
      #   redirect_to "/resendtokenpage", :notice => "Your account has not be activated."
      # end

      session[:user_id] = @current_user_confirm_id
      session[:status] = @current_user_status
      session[:expires_at] = Time.current + 1.hours


      @db.query("UPDATE users SET updated_at=now() WHERE email='"+@email.to_s+"'")


      data = {:message => "Logged In", :user_id => @current_user_confirm_id, :status => "true"}


      if @current_user.is_admin?
        redirect_to "/dashboard"
      else
        redirect_to "/home"
      end

      # respond_to do |format|
        # format.html { redirect_to "/home" }
        # format.json { render :json => data, :status => :ok }
      # end
  end

  def logout

      session[:user_id] = nil
      session[:status] = nil

      return redirect_to "/"
  end

  def home
    #to be coded, get back the test records.

    @current_user = User.find(session[:user_id])


    # response waiting rating
    @complete_tests = TestRecord.where(:status => 1, :cheating => 0).group_by(&:photo_verification)

	   #return render :json => @complete_tests

    @wait_for_verify_tests = @complete_tests[0]
    @verify_passed_tests = @complete_tests[1]

    # data = {:message => @verify_passed_tests, :status => "false"}
    # return render :json => data, :status => :ok

    @available_responses = []
    @english_responses = []
    @spanish_responses = []

    if !@verify_passed_tests.nil?

      @verify_passed_tests.each do |verify_passed_test|

       #  data = {:message => @verify_passed_test, :status => "false"}
       # return render :json => data, :status => :ok

        response = Response.where(:status => 0, :test_record_id => verify_passed_test.id).order("test_record_id asc, level desc").first

        # data = {:message => response, :status => "false"}
        # return render :json => data, :status => :ok

	if response
          if @prompt = Prompt.find_by_id(response.prompt_id)
            if @bmr = Response.find_by_id(@prompt.bmr)
            logger.info @prompt
            logger.info @bmr

          if response.raters.nil? || !response.raters.include?(@current_user.id.to_s)
            @available_responses << response
            if response.language == "English"
              @english_responses << response
            else
              @spanish_responses << response
            end
          end

            end
          end
	end
      end

    end

    # @available_responses = @available_responses.last(6).reverse

    # data = {:message => @verify_passed_tests, :status => "false"}
    # return render :json => data, :status => :ok

    # latest tests
    @latest_tests = Test.where(:status => 1, :is_practice => false).last(6).reverse
    @test_records = TestRecord.where(:user_id => @current_user.id).order(created_at: :desc)

  end

  def dashboard
    if !@current_user.admin?
      return redirect_to '/home'
    end

    # date
    @date_from  = Date.parse("2017-06-01").beginning_of_month
    @date_previous_month = Date.today.beginning_of_month - 1.month
    @date_to    = Date.today.beginning_of_month
    @date_range = @date_from..@date_to
    @month_range = @date_previous_month..@date_to

    @date_months = @date_range.map {|d| Date.new(d.year, d.month, 1) }.uniq
    @date_months = @date_months.map {|d| d.strftime "%Y-%-m" }
    # date_months = date_months.values_at(* date_months.each_index.select {|i| i.even?})


    # user data
    @users = User.group("year(created_at)", "month(created_at)").count
    @users_time = []
    @users_count = []
    @users.keys.each do |k|
      @users_time << k[0].to_s+ '-' + k[1].to_s
    end
    @users.values.each do |v|
      @users_count << v
    end
    @users_data = Hash[*@users_time.zip(@users_count).flatten]
    # @users_time.each_with_index { |@users_time, @users_count| @users_data[@users_time] = value[@users_count] }

    @users_hash = Hash[@date_months.map {|x| [x, 0]}]
    @users_hash = @users_hash.merge(@users_data){|key,val1,val2| val1 + val2}

    # new user count
    @new_users = User.where(:created_at => @month_range).count

    # rater count
    @raters = User.where(:is_rater => true).group("year(created_at)", "month(created_at)").count
    @raters_time = []
    @raters_count = []
    @raters.keys.each do |k|
      @raters_time << k[0].to_s+ '-' + k[1].to_s
    end
    @raters.values.each do |v|
      @raters_count << v
    end
    @raters_data = Hash[*@raters_time.zip(@raters_count).flatten]

    @raters_hash = Hash[@date_months.map {|x| [x, 0]}]
    @raters_hash = @raters_hash.merge(@raters_data){|key,val1,val2| val1 + val2}

    @total_raters = User.where(:is_rater => true).count


    # test data
    @ilr1_records = TestRecord.where(:score => 1).group("year(updated_at)", "month(updated_at)").count
    @ilr1_records_time = []
    @ilr1_records_count = []
    @ilr1_records.keys.each do |k|
      @ilr1_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @ilr1_records.values.each do |v|
      @ilr1_records_count << v
    end
    @ilr1_records_data = Hash[*@ilr1_records_time.zip(@ilr1_records_count).flatten]

    @ilr1_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @ilr1_records_hash = @ilr1_records_hash.merge(@ilr1_records_data){|key,val1,val2| val1 + val2}

    @ilr1plus_records = TestRecord.where(:score => 2).group("year(updated_at)", "month(updated_at)").count
    @ilr1plus_records_time = []
    @ilr1plus_records_count = []
    @ilr1plus_records.keys.each do |k|
      @ilr1plus_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @ilr1plus_records.values.each do |v|
      @ilr1plus_records_count << v
    end
    @ilr1plus_records_data = Hash[*@ilr1plus_records_time.zip(@ilr1plus_records_count).flatten]

    @ilr1plus_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @ilr1plus_records_hash = @ilr1plus_records_hash.merge(@ilr1plus_records_data){|key,val1,val2| val1 + val2}

    @ilr2_records = TestRecord.where(:score => 3).group("year(updated_at)", "month(updated_at)").count
    @ilr2_records_time = []
    @ilr2_records_count = []
    @ilr2_records.keys.each do |k|
      @ilr2_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @ilr2_records.values.each do |v|
      @ilr2_records_count << v
    end
    @ilr2_records_data = Hash[*@ilr2_records_time.zip(@ilr2_records_count).flatten]

    @ilr2_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @ilr2_records_hash = @ilr2_records_hash.merge(@ilr2_records_data){|key,val1,val2| val1 + val2}

    @ilr2plus_records = TestRecord.where(:score => 4).group("year(updated_at)", "month(updated_at)").count
    @ilr2plus_records_time = []
    @ilr2plus_records_count = []
    @ilr2plus_records.keys.each do |k|
      @ilr2plus_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @ilr2plus_records.values.each do |v|
      @ilr2plus_records_count << v
    end
    @ilr2plus_records_data = Hash[*@ilr2plus_records_time.zip(@ilr2plus_records_count).flatten]

    @ilr2plus_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @ilr2plus_records_hash = @ilr2plus_records_hash.merge(@ilr2plus_records_data){|key,val1,val2| val1 + val2}

    @ilr3_records = TestRecord.where(:score => 5).group("year(updated_at)", "month(updated_at)").count
    @ilr3_records_time = []
    @ilr3_records_count = []
    @ilr3_records.keys.each do |k|
      @ilr3_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @ilr3_records.values.each do |v|
      @ilr3_records_count << v
    end
    @ilr3_records_data = Hash[*@ilr3_records_time.zip(@ilr3_records_count).flatten]

    @ilr3_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @ilr3_records_hash = @ilr3_records_hash.merge(@ilr3_records_data){|key,val1,val2| val1 + val2}

    #  data = {:hash1 => @ilr1plus_records_hash, :status => "false"}
    #  return render :json => data, :status => :ok

    # payment data
    @payment_records = PaymentRecord.group("year(created_at)").group("month(created_at)").count
    @payment_records_time = []
    @payment_records_count = []
    @payment_records.keys.each do |k|
      @payment_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @payment_records.values.each do |v|
      @payment_records_count << v
    end
    @payment_records_data = Hash[*@payment_records_time.zip(@payment_records_count).flatten]

    @payment_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @payment_records_hash = @payment_records_hash.merge(@payment_records_data){|key,val1,val2| val1 + val2}

    # caculate revenue
    @revenue = PaymentRecord.all
    if !@revenue.nil?
      @sum = 0
      @revenue.each do |revenue|
        @sum = @sum + revenue.price.to_d
      end

      @sum = @sum.to_s
      @total_revenue = @sum
    else
      @sum = "0.00"
      @total_revenue = @sum
    end

    #  data = {:hash1 => @payment_records_hash, :status => "false"}
    #  return render :json => data, :status => :ok

    # rater paycheck
    @payout_records = PayoutRecord.group("year(created_at)").group("month(created_at)").count
    @payout_records_time = []
    @payout_records_count = []
    @payout_records.keys.each do |k|
      @payout_records_time << k[0].to_s+ '-' + k[1].to_s
    end
    @payout_records.values.each do |v|
      @payout_records_count << v
    end
    @payout_records_data = Hash[*@payout_records_time.zip(@payout_records_count).flatten]

    @payout_records_hash = Hash[@date_months.map {|x| [x, 0]}]
    @payout_records_hash = @payout_records_hash.merge(@payout_records_data){|key,val1,val2| val1 + val2}

    # caculate total paycheck
    @paycheck = PayoutRecord.all
    if !@paycheck.nil?
      @total = 0
      @paycheck.each do |paycheck|
        @total = @total + paycheck.amount.to_d
      end

      @total = @total.to_s
      @total_paycheck = @total
    else
      @total = "0.00"
      @total_paycheck = @total
    end

    # total balannce
    @total_balance = @sum.to_d - @total.to_d
    @total_balance = @total_balance.to_s

    #  data = {:hash1 => @payout_records_data, :status => "false"}
    #  return render :json => data, :status => :ok
  end

  def cashflow
    @payment_records = PaymentRecord.paginate(:page => params[:page], :per_page => 15)
    @statement = PaymentRecord.all

    respond_to do |format|
      format.html
      format.csv { send_data @statement.to_csv, filename: "payment records-#{Date.today}.csv" }
      format.pdf { send_data @statement.to_pdf, filename: "payment records-#{Date.today}.pdf" }
    end
  end

  def invite

  end

  def invite_user
    @email = params[:invite_email]
    @subject = params[:invite_subject]
    @message = params[:invite_content]

    if @email == "" || @subject == "" || @message == ""
      return redirect_to "/invite", notice: "Please fill all required fields"
    end

    # this part is the 3rd party mail function, need to setup when we get momre info from the domain
    mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
    mb_obj = Mailgun::MessageBuilder.new()

    # @user.email.to_s
    mb_obj.set_from_address("do-not-reply@parrot.com", {"first"=>"Parrot 66", "last" => ""});
    mb_obj.add_recipient(:to, @email, @email);

    mb_obj.set_subject(@subject);
    # mb_obj.set_text_body(result_msg);

    result_msg = "<p>Dear "+@email+"</p>"
    result_msg += "<p>"+@message+"</p><br />"
    result_msg += "<a href='https://www.parrot66.com'>Parrot66</a><br />"

    result_msg += "<p></p>"
    result_msg += "<p>Kind Regards,</p>"


    mb_obj.set_html_body(result_msg);
    mg_client.send_message("parrot66.com", mb_obj)
    # this part is the 3rd party mail function, need to setup when we get momre info from the domain

    redirect_to users_path, notice: "The invitation has been sent."

  end

  def invite_as_rater
    @user = User.find_by_id(params[:user_id])

     # data = {:user => @user, :status => "false"}
     # return render :json => data, :status => :ok

    @user.update_attributes(:is_rater => 1, :weekly_rated => 0, :total_rated => 0)

    # this part is the 3rd party mail function, need to setup when we get momre info from the domain
    mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
    mb_obj = Mailgun::MessageBuilder.new()

    # @user.email.to_s
    mb_obj.set_from_address("do-not-reply@parrot.com", {"first"=>"Parrot 66", "last" => ""});
    mb_obj.add_recipient(:to, @user.email, {"first" => @user.f_name.to_s, "last" => @user.l_name.to_s});

    mb_obj.set_subject("P66 | Become a Rater");
    # mb_obj.set_text_body(result_msg);

    result_msg = "<p>Hi "+@user.f_name.to_s+" "+@user.l_name.to_s+"</p>"
    result_msg += "<p>Congratulations! You have met the requirement to join the exclusive family of Parrot Raters!</p><br />"
    result_msg += "<p>In under one minute you can now start earning money from your mobile phone, tablet or computer! The best part is you will be helping others get the Parrot credential they need to help support themselves and the language you share!</p><br />"
    result_msg += "<p>Your opportunity is now! Work whenever you want - just click on the link below to get started!</p><br />"
    result_msg += "<p></p><br />"
    result_msg += "<a href='https://www.parrot66.com'>Parrot66</a><br />"
    result_msg += "<p></p><br />"

    result_msg += "<p>Best,</p><br />"
    result_msg += "<p>Parrot66</p>"



    mb_obj.set_html_body(result_msg);
    mg_client.send_message("parrot66.com", mb_obj)
    # this part is the 3rd party mail function, need to setup when we get momre info from the domain

    redirect_to users_path

  end

  def rating_history

  end

  def index
    if !@current_user.admin?
      return redirect_to '/home'
    end
    @users = User.paginate(:page => params[:page], :per_page => 15)

  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    @test_records = TestRecord.where(:user_id => @user.id, :status => 2)
    # @test_records = TestRecord.where(:user_id => @user.id, :cheating => 0).paginate(:page => params[:page], :per_page => 5)

    # data = {:message => @test_records, :status => "false"}
    # return render :json => data, :status => :ok

    respond_to do |format|
      format.html
      format.js
    end

  end

  # GET /users/new

  def new
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]

    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @current_user = User.find(session[:user_id])
  end

  # POST /users
  # POST /users.json
  def create

    @user = User.new(user_params)

    if @user.f_name.to_s == "" || @user.email.to_s == "" || @user.email_confirmation.to_s == "" || @user.password.to_s == "" || @user.password_confirmation.to_s == ""
        return redirect_to new_user_path, notice: "Please make sure you input all require field."
    end

    if (params[:user]["date_of_birth(1i)"] != "" && params[:user]["date_of_birth(2i)"] != "" && params[:user]["date_of_birth(3i)"] != "")
      date_of_birth = Date.civil(params[:user]["date_of_birth(1i)"].to_i, params[:user]["date_of_birth(2i)"].to_i, params[:user]["date_of_birth(3i)"].to_i)
      if date_of_birth > Date.today
        return redirect_to new_user_path, notice: "Please check the Date of Birth you entered"
      end
    end

    #logger.info params[:user]["date_of_birth(1i)"].to_i
    #logger.info params[:user]["date_of_birth(2i)"].to_i
    #logger.info params[:user]["date_of_birth(3i)"].to_i

    #begin
      #date_of_birth = Date.civil(params[:user]["date_of_birth(1i)"].to_i, params[:user]["date_of_birth(2i)"].to_i, params[:user]["date_of_birth(3i)"].to_i)
    #rescue
      #return redirect_to new_user_path, notice: "please make sure the date of birth"
    #end

    #if date_of_birth > Date.today
      #return redirect_to new_user_path, notice: "Please check the Date of Birth you entered"
    #end

    if @user.email.to_s != @user.email_confirmation.to_s
      return redirect_to new_user_path, notice: "Please make sure the email address is the same."
    end

    @email_password = @user.password.to_s

    if @user.password.to_s != @user.password_confirmation.to_s
        return redirect_to new_user_path, notice: "Please make sure the password is the same."
    end

    @exist_user = User.find_by_email(@user.email)

    if !@exist_user.nil?
        return redirect_to new_user_path, notice: "Sorry, User exist, please use any other email."
    end

    @rand_list = [('0'..'9'),('A'..'Z')].map(&:to_a).flatten
    @user.parrot_id = (1..6).map{ @rand_list[rand(@rand_list.length)] }.join

    @token = SecureRandom.uuid + "-" + rand(1000).to_s + "-" + SecureRandom.uuid

    @user.password = BCrypt::Password.create(@user.password)
    @user.token = @token.to_s
    @user.status = "0"

    # this part is the 3rd party mail function, need to setup when we get momre info from the domain
    mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
    mb_obj = Mailgun::MessageBuilder.new()

    # @user.email.to_s
    mb_obj.set_from_address("do-not-reply@parrot.com", {"first"=>"Parrot 66", "last" => ""});
    mb_obj.add_recipient(:to, @user.email, {"first" => @user.f_name.to_s, "last" => @user.l_name.to_s});

    # mb_obj.set_subject("Parrot | Automatic Welcome Email");
    # mb_obj.set_text_body(result_msg);

    # result_msg = "<p>Dear "+@user.f_name.to_s+" "+@user.l_name.to_s+"</p>"
    # result_msg += "<p>Thank you so much for registering at Parrot! To activate your account, please click on the link below:</p>"
    # result_msg += "<a href='"+request.host+"/activeuser?token="+@token.to_s+"'>https://"+request.host+"/activeuser?token="+@token.to_s+"</a>"
    # result_msg += "<p></p>"
    # result_msg += "<p>This Link will expire in 24 hours from now</p>"
    # result_msg += "<p></p>"
    # result_msg += "<p>Username : "+@user.email.to_s+"<br />"
    # result_msg += "Password: "+@email_password.to_s+"</p>"
    # result_msg += "<p></p>"
    # result_msg += "<p>Kind Regards, The Parrot Team</p>"
    # result_msg += "<p>Thank you for registering with Parrot!</p>"

    mb_obj.set_subject("Parrot Account Confirmation");

    result_msg = "<p>Hi "+@user.f_name.to_s+"</p>"
    result_msg += "<p>Thank you for signing up with Parrot Language Tests! We are sending yout this email to let you know your account is ready to go. We know you will love your experience with us, but if anything should happen that is less than fantastic, please let us know so we can make things right.</p>"
    result_msg += "<p>At Parrot, \"you\" are always the priority.</p>"
    result_msg += "<a href='"+request.host+"/activeuser?token="+@token.to_s+"'>Login Now</a>"
    result_msg += "<p>-The Parrot Team</p>"


    mb_obj.set_html_body(result_msg);
    mg_client.send_message("parrot66.com", mb_obj)
    # this part is the 3rd party mail function, need to setup when we get momre info from the domain

    session[:sign_up_email] = @user.email

    respond_to do |format|
      if @user.save
        # format.html { redirect_to thankyou_path, notice: 'User was successfully created.' }
        format.html { redirect_to thankyou_path, email: @user.email}
        # format.json { render :show, status: :created, location: @user }
      else
        format.html { redirect_to new_user_path }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end



  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @current_user = User.find(session[:user_id])

    # data = {:message => @current_user, :status => "false"}
    # return render :json => data, :status => :ok

    if (params[:user]["date_of_birth(1i)"] != "" && params[:user]["date_of_birth(2i)"] != "" && params[:user]["date_of_birth(3i)"] != "")
      date_of_birth = Date.civil(params[:user]["date_of_birth(1i)"].to_i, params[:user]["date_of_birth(2i)"].to_i, params[:user]["date_of_birth(3i)"].to_i)
      if date_of_birth > Date.today
        return redirect_to "/account", notice: "Please check the Date of Birth you entered"
      end
    end

    #logger.info params[:user]["date_of_birth(1i)"].to_i
    #logger.info params[:user]["date_of_birth(2i)"].to_i
    #logger.info params[:user]["date_of_birth(3i)"].to_i

    #begin
      #date_of_birth = Date.civil(params[:user]["date_of_birth(1i)"].to_i, params[:user]["date_of_birth(2i)"].to_i, params[:user]["date_of_birth(3i)"].to_i)
    #rescue
      #return redirect_to "/account", notice: "please make sure the date of birth"
    #end

    #if date_of_birth > Date.today
      #return redirect_to "/account", notice: "Please check the Date of Birth you entered"
    #end

    if params[:user][:password] != params[:user][:password_confirmation]
      return redirect_to "/account", notice: "please make sure the password is the same"
    end

    @encrypted_password = BCrypt::Password.new(@current_user.password)
    @auth = (@encrypted_password == params[:user][:password])

    if @auth == true
      return redirect_to "/account", notice: "can not use the same password"
    end

    is_change_password = false

    if params[:user][:password].to_s == "" || params[:user][:password_confirmation].to_s == ""
      params[:user][:password] = @current_user.password
    else
      params[:user][:password] = BCrypt::Password.create(params[:user][:password])
      is_change_password = true;
    end

    # @rand_list = [('0'..'9'),('A'..'Z')].map(&:to_a).flatten
    # params[:user][:parrot_id] = (1..6).map{ @rand_list[rand(@rand_list.length)] }.join

    # data = {:message => params[:user][:password], :status => "false"}
    # return render :json => data, :status => :ok

    respond_to do |format|
      if @current_user.update(user_params)
        # data = {:message => params[:user][:password], :status => "false"}
        # return render :json => data, :status => :ok
        if(is_change_password)
          session[:user_id] = nil
          session[:status] = nil
          format.html { redirect_to "/?status=changedpassword" }
          format.json { render :show, status: :ok }
        else
          format.html { redirect_to "/account", notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @current_user }
        end
      else
        format.html { render :edit }
        format.json { render json: @current_user.errors, status: :unprocessable_entity }
      end
    end
    #logout
  end

  # def change_password
  #   @current_user = User.find(session[:user_id])
  #
  #   @password = params[:password]
  #   @password_confirmation = params[:password_confirmation]
  #
  #   if @password != @password_confirmation
  #     return redirect_to "/account", notice: "please make sure the password is the same"
  #   end
  #
  #   @encrypted_password = BCrypt::Password.new(@current_user.password)
  #   @auth = (@encrypted_password == @password)
  #
  #   if @auth == true
  #     return redirect_to "/account", notice: "can not use the same password"
  #   end
  #
  #   if @password.to_s == "" || @password_confirmation.to_s == ""
  #     @password = @current_user.password
  #   else
  #     @password = BCrypt::Password.create(@password)
  #   end
  #
  #   respond_to do |format|
  #     @current_user.update_attribute(:password, @password)    result_msg += "<p></p>"
    # result_msg += "<p>Username : "+@user.email.to_s+"<br />"
    # result_msg += "Password: "+@email_password.to_s+"</p>"
    # result_msg += "<p></p>"
    # result_msg += "<p>Kind Regards, The Parrot Team</p>"
    # result_msg += "<p>Thank you for registering with Parrot!</p>"
  #     format.html { redirect_to "/account", notice: 'Password changed.' }
  #     format.json { head :no_content }
  #   end
  # end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def change_user_status
    @user_status = params[:user_status]
    @user = User.find_by_id(params[:user_id])

    # data = {:message => @user, :status => "false"}
    # return render :json => data, :status => :ok

    @user.update_attribute(:status, @user_status)
  end

  def cheating
    @user = User.find(params[:id])

    if @user.cheating.nil?
      @cheating_records = []
    else
      @cheating_records = @user.cheating
    end

    # data = {:message => @cheating_records[0][0], :status => "false"}
    # return render :json => data, :status => :ok

    respond_to do |format|
      format.html
      format.js
    end

  end

  # cms
  def cms
    if !@current_user.admin?
      redirect_to '/home'
    end

    @pages = Page.all
  end

  def page_edit
    if !@current_user.admin?
      redirect_to '/home'
    end
    @page = Page.find_by_id(params[:id])
  end

  def upload_image
    # Take upload from params[:file] and store it somehow...
    # Optionally also accept params[:hint] and consume if needed
    image = Image.create params.permit(:file, :alt, :hint)

    render json: {
      image: {
        url: image.file.url
      }
    }, content_type: "text/html"
  end

  def content_update
    @page = Page.find_by_id(params[:id])

    respond_to do |format|
      if @page.update(page_params)
        # data = {:message => @page, :status => "false"}
        # return render :json => data, :status => :ok
        format.html { redirect_to "/cms", notice: 'Page was successfully updated.' }
        format.json { render :cms, status: :ok, location: @page }
      else
        format.html { render :content_update, alert: 'Page update failed.' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def download_certificate_pdf

    if !@current_user.nil?
      certificate = CertificatePdf.new(@current_user, params[:id])
      certificate.to_pdf

      send_file(
        "#{Rails.root}/public/certificate.pdf",
        filename: "certificate.pdf",
        type: "application/pdf"
      )
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_user
    #   @user = User.find(session[:user_id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.fetch(:user, {})

      params.require(:user).permit(:f_name, :email, :email_confirmation, :l_name, :native_language, :date_of_birth, :gender, :industry, :password, :password_confirmation, :avatar, :avatar_cache, :status, :token, :parrot_id, :recipient_type, :receiver)
    end

    def page_params
      params.fetch(:page, {})

      params.require(:page).permit(:title, :slug, :body, :section1, :section2, :section3, :section4, :section5, :section6, :section7, :section8, :section9, :image1, :image2, :image3, :image4, :page_type)
    end
end
