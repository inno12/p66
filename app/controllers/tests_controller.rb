class TestsController < ApplicationController
  layout 'user'

  before_action :set_test, only: [:show, :edit, :update, :destroy]
  #before_action :authuser, except: [:add_to_cart]

  # to make Practice Test button work on aboutthetests page by Johann, part 1/2
  before_action :authuser, except: [:add_to_cart, :take_practice_test, :show]


  # GET /tests
  # GET /tests.json
  def index
    if !@current_user.admin?
      return redirect_to '/home'
    end

    @tests = Test.paginate(:page => params[:page], :per_page => 10)
    @popularity = TestRecord.group("test_id").count

    # to show prompt deails on index page

    # data = {:message => @popularity, :status => "false"}
    # return render :json => data, :status => :ok

  end

  # GET /tests/1
  # GET /tests/1.json
  def show
    @test = Test.find(params[:id])
    @test_record = TestRecord.find_by_id(session[:test_record])

    if @test_record.status == 1
      return redirect_to test_records_path, :notice => "The test has been taken."
    end

    @test_record.update_attribute(:start_date, DateTime.now)

    @ilr1_prompt1 = Prompt.find_by_prompt_id(@test.ilr1_prompt1.to_i)
    @ilr1_prompt2 = Prompt.find_by_prompt_id(@test.ilr1_prompt2.to_i)
    @ilr1plus_prompt1 = Prompt.find_by_prompt_id(@test.ilr1plus_prompt1.to_i)
    @ilr1plus_prompt2 = Prompt.find_by_prompt_id(@test.ilr1plus_prompt2.to_i)
    @ilr2_prompt1 = Prompt.find_by_prompt_id(@test.ilr2_prompt1.to_i)
    @ilr2_prompt2 = Prompt.find_by_prompt_id(@test.ilr2_prompt2.to_i)
    @ilr2plus_prompt1 = Prompt.find_by_prompt_id(@test.ilr2plus_prompt1.to_i)
    @ilr2plus_prompt2 = Prompt.find_by_prompt_id(@test.ilr2plus_prompt2.to_i)
    @ilr3_prompt1 = Prompt.find_by_prompt_id(@test.ilr3_prompt1.to_i)
    @ilr3_prompt2 = Prompt.find_by_prompt_id(@test.ilr3_prompt2.to_i)

    @current_slide = 0

    if !@test_record.nil? && !@test_record.responses.nil? && @test_record.responses.count > 0
      test_response = @test_record.responses.last

      if(!test_response.prompt_id.nil?)
        @current_slide = 0 if test_response.prompt_id == @ilr1_prompt1.id
        @current_slide = 1 if test_response.prompt_id == @ilr1_prompt2.id
        @current_slide = 2 if test_response.prompt_id == @ilr1plus_prompt1.id
        @current_slide = 3 if test_response.prompt_id == @ilr1plus_prompt2.id
        @current_slide = 4 if test_response.prompt_id == @ilr2_prompt1.id
        @current_slide = 5 if test_response.prompt_id == @ilr2_prompt2.id
        @current_slide = 6 if test_response.prompt_id == @ilr2plus_prompt1.id
        @current_slide = 7 if test_response.prompt_id == @ilr2plus_prompt2.id
        @current_slide = 8 if test_response.prompt_id == @ilr3_prompt1.id
        @current_slide = 9 if test_response.prompt_id == @ilr3_prompt2.id
      end
    end

    # data = {:message => @test_record.responses, :status => "false"}
    # render :json => data, :status => :ok

  end

  # GET /tests/new
  def new
    if !@current_user.admin?
      return redirect_to '/home'
    end

    @test = Test.new
    @prompts = Prompt.all

    @all_prompt_hash = {}
    @all_lang_prompt_hash = {}
    @all_industry_prompt_hash = {}
    @all_level_prompt_hash = {}

    if @prompts.count.to_i > 0
        @prompts.each do |prompt|
            @all_prompt_hash[prompt["id"]] = prompt

            if @all_lang_prompt_hash[prompt["language"]].nil?
                @all_lang_prompt_hash[prompt["language"]] = []
            end

            if @all_industry_prompt_hash[prompt["industry"]].nil?
                @all_industry_prompt_hash[prompt["industry"]] = []
            end

            if @all_level_prompt_hash[prompt["level"]].nil?
                @all_level_prompt_hash[prompt["level"]] = []
            end

            @all_lang_prompt_hash[prompt["language"]] << prompt["id"]
            @all_industry_prompt_hash[prompt["industry"]] << prompt["id"]
            @all_level_prompt_hash[prompt["level"]] << prompt['id']

        end
    end

    # data = {:message => @all_lang_prompt_hash.to_json, :status => "false"}
    # render :json => data, :status => :ok

  end

  # GET /tests/1/edit
  def edit
    if !@current_user.admin?
      return redirect_to '/home'
    end

    @test = Test.find_by_id(params[:id])
    @prompts = Prompt.all

    @all_prompt_hash = {}
    @all_lang_prompt_hash = {}
    @all_industry_prompt_hash = {}
    @all_level_prompt_hash = {}

    if @prompts.count.to_i > 0
        @prompts.each do |prompt|
            @all_prompt_hash[prompt["id"]] = prompt

            if @all_lang_prompt_hash[prompt["language"]].nil?
                @all_lang_prompt_hash[prompt["language"]] = []
            end

            if @all_industry_prompt_hash[prompt["industry"]].nil?
                @all_industry_prompt_hash[prompt["industry"]] = []
            end

            if @all_level_prompt_hash[prompt["level"]].nil?
                @all_level_prompt_hash[prompt["level"]] = []
            end

            @all_lang_prompt_hash[prompt["language"]] << prompt["id"]
            @all_industry_prompt_hash[prompt["industry"]] << prompt["id"]
            @all_level_prompt_hash[prompt["level"]] << prompt['id']

        end
    end
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = Test.new

    @test[:status] = params[:status].to_i
    @test[:is_practice] = params[:is_practice].to_i
    @test[:title] = params[:title]
    @test[:language] = params[:language]
    @test[:industry] = params[:industry]
    @test[:description] = params[:description]
    @test[:time] = params[:time]
    @test[:price] = params[:price]
    @test[:ilr1_prompt1] = params[:ilr1_prompt1]
    @test[:ilr1_prompt2] = params[:ilr1_prompt2]
    @test[:ilr1plus_prompt1] = params[:ilr1plus_prompt1]
    @test[:ilr1plus_prompt2] = params[:ilr1plus_prompt2]
    @test[:ilr2_prompt1] = params[:ilr2_prompt1]
    @test[:ilr2_prompt2] = params[:ilr2_prompt2]
    @test[:ilr2plus_prompt1] = params[:ilr2plus_prompt1]
    @test[:ilr2plus_prompt2] = params[:ilr2plus_prompt2]
    @test[:ilr3_prompt1] = params[:ilr3_prompt1]
    @test[:ilr3_prompt2] = params[:ilr3_prompt2]

    # data = {:message => @test, :status => "false"}
    # render :json => data, :status => :ok

    if @test.ilr1_prompt1.nil? || @test.ilr1_prompt2.nil? || @test.ilr1plus_prompt1.nil? || @test.ilr1plus_prompt2.nil? || @test.ilr2_prompt1.nil? || @test.ilr2_prompt2.nil? || @test.ilr2plus_prompt1.nil? || @test.ilr2plus_prompt2.nil? || @test.ilr3_prompt1.nil? || @test.ilr3_prompt2.nil? || @test.title == "" || @test.language == "" || @test.industry == "" || @test.description == "" || @test.time == "" || @test.price == ""
      return redirect_to new_test_path, notice: "Please fill in all required fields"
    end

    respond_to do |format|
      if @test.save
        format.html { redirect_to tests_path, notice: 'Test was successfully created.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    @test = Test.find_by_id(params[:id])

    # data = {:params => params, :test => @test, :status => "false"}
    # return render :json => data, :status => :ok

    # @test.status = params[:status].to_i
    # @test.is_practice = params[:is_practice].to_i
    # @test.title = params[:title]
    # @test.language = params[:language]
    # @test.industry = params[:industry]
    # @test.description = params[:description]
    # @test.time = params[:time]
    # @test.price = params[:price]
    # @test.ilr1_prompt1 = params[:ilr1_prompt1]
    # @test.ilr1_prompt2 = params[:ilr1_prompt2]
    # @test.ilr1plus_prompt1 = params[:ilr1plus_prompt1]
    # @test.ilr1plus_prompt2 = params[:ilr1plus_prompt2]
    # @test.ilr2_prompt1 = params[:ilr2_prompt1]
    # @test.ilr2_prompt2 = params[:ilr2_prompt2]
    # @test.ilr2plus_prompt1 = params[:ilr2plus_prompt1]
    # @test.ilr2plus_prompt2 = params[:ilr2plus_prompt2]
    # @test.ilr3_prompt1 = params[:ilr3_prompt1]
    # @test.ilr3_prompt2 = params[:ilr3_prompt2]

    # data = {:params => params, :title => params[:title].to_s, :description => params[:description].to_s, :language => params[:language], :test => @test, :status => "false"}
    # return render :json => data, :status => :ok

    respond_to do |format|
      if @test.update(test_params)
        format.html { redirect_to "/tests", notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to tests_url, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def add_to_cart
    @test = params[:test]
    session[:tests] ||= []
    session[:tests] << @test
    session[:cart_expires_at] = Time.current + 2.hours
    data = {:message => session[:tests], :status => "true"}
    render :json => data, :status => :ok
  end

  # def add_to_cart
  #   @cart_items = session[:tests]
  #   if !@cart_items.nil?
  #     @cart_items = @cart_items.map{|ci| ci.to_i}
  #     @cart_items.each do |cart_item|
  #       current_cart.add_test_to_cart(cart_item)
  #     end
  #
  #     session[:tests] = nil
  #     $tests = []
  #     data = {:message => "added to cart", :cart => current_cart, :status => "true"}
  #     render :json => data, :status => :ok
  #     return redirect_to carts_path
  #
  #   else
  #     return redirect_to carts_path
  #   end
  # end

  def buy_test_after_login
    @test = Test.find_by_id(params[:test])
    session[:tests] ||= []

    session[:tests] << @test.id
    session[:total_price] = @test.price

    redirect_to checkout_path

  end

  def close_test
    @test_record = TestRecord.find_by_id(params[:test_record_id])

    # data = {:message => @test_record :status => "false"}
    # render :json => data, :status => :ok

    # @test_record.update_attribute(:status => 1)
    redirect_to "/finished_test"
  end

  def window_lose_focus

    data = {:message => session[:tests], :status => "true"}
    render :json => data, :status => :ok
    return
    # @test_record = TestRecord.find_by_id(params[:test_record_id])
    # @cheating_record = [@test_record.id, "Window lost focus detected.", DateTime.now.to_date]
    #
    # @test_record.update_attribute(:cheating, 1)
    #
    # #added test fail messages
    #
    # if @current_user.cheating.nil?
    #   @cheating_records = []
    #   @cheating_records << @cheating_record
    #   @current_user.update_attribute(:cheating, @cheating_records)
    #
    #   redirect_to test_records_path, :notice => "Please be aware that the Parrot66 language test is delivered over a secure platform. In addition to identification confirmation measures, the platform will detect any attempts to access a third party application during the actual test and result in a resetting of your test. Should this happen more than twice, your test will be blocked and you will not be able to complete your test."
    #
    # else
    #   @cheating_records = @current_user.cheating
    #   @cheating_records << @cheating_record
    #   @current_user.update_attribute(:cheating, @cheating_records)
    #
    #   if@cheating_records.size<2
    #     redirect_to test_records_path, :notice => "Your test has been stopped due to a security breach. While the test is active, please be sure not to click outside of the Parrot tab for any reason. You can log back in"
    #   else
    #     redirect_to test_records_path, :notice => "Your test has been stopped due to a security breach. You have clicked outside of the Parrot tab for the third time and your test has now been blocked. The Parrot Team will review your test record and be in touch with you shortly."
	  #   end
    # end

    # redirect_to test_records_path, :notice => "Test Failed."

  end

  def test_brief
    @test = Test.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def test_checkout
    @test = Test.find_by_id(params[:test_id])
    session[:tests] = [@test.id]
    session[:total_price] = params[:price]


    # data = {:message => session[:tests], :status => "true"}
    # render :json => data, :status => :ok

    redirect_to checkout_path
  end

  def take_practice_test
    test = Test.find_by_id(params[:id])
    # session[:tests] = [@test.id]
    # session[:total_price] = params[:price]

    test_record = TestRecord.new

    # to make Practice Test button work on aboutthetests page by Johann, part 2/2
    if !session[:user_id]
      test_record[:user_id] = 4 # set user ID to store record of guest
    else
      @current_user = User.find_by_id(session[:user_id])
      test_record[:user_id] = @current_user.id
    end

    test_record[:test_id] = test.id
    test_record[:test_name] = test.title
    test_record[:language] = test.language
    test_record[:industry] = test.industry
    test_record[:purchased_date] = Time.now

    test_record.save
    session[:test_record] = test_record.id

    # data = {:message => test_record, :status => "false"}
    # render :json => data, :status => :ok
    redirect_to test_path(test.id)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.fetch(:test, {})

      params.require(:test).permit(:title, :description, :price, :time, :status, :is_practice, :language, :industry, :ilr1_prompt1, :ilr1_prompt2, :ilr1plus_prompt1, :ilr1plus_prompt2, :ilr2_prompt1, :ilr2_prompt2, :ilr2plus_prompt1, :ilr2plus_prompt2, :ilr3_prompt1, :ilr3_prompt2)
    end
end
