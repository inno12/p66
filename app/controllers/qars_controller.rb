class QarsController < ApplicationController
  before_action :set_qar, only: [:show, :edit, :update, :destroy]
  before_action :authuser

  # GET /qars
  # GET /qars.json
  def index
    @qars = Qar.paginate(:page => params[:page], :per_page => 15)
  end

  # GET /qars/1
  # GET /qars/1.json
  def show
    if !@current_user.rater? && !@current_user.admin?
      return redirect_to "/home"
    end

    @qar = Qar.find(params[:id])

    if !@qar.raters.nil? && @qar.raters.include?(@current_user.id.to_s)
      return redirect_to "/rater_available", :notice => "The response has been rated."
    end

    @qar.lock!


    @prompt = Prompt.find_by_id(@qar.prompt.to_i)
    @bmr = Response.find_by_id(@qar.bmr.to_i)
    @response = Response.find_by_id(@qar.response.to_i)

  end

  def qar_submit
    @qar = Qar.find_by_id(params[:qar_id])
    @rating = params[:rating]
    @rater_id = @current_user.id

    if @rating == "0"
        return redirect_to qar_path(@qar), alert: "Please select a rating"
    end

    case @rating
    when "better than"
      if @qar.rating == "worse than"
        if @current_user.qar_fails.nil?
          @current_user.update_attribute(:qar_fails, 1)
        else
          @qar_fails = @current_user.qar_fails + 1
          @current_user.update_attribute(:qar_fails, @qar_fails)
        end
      end
    when "as good as"
      if @qar.rating == "worse than"
        if @current_user.qar_fails.nil?
          @current_user.update_attribute(:qar_fails, 1)
        else
          @qar_fails = @current_user.qar_fails + 1
          @current_user.update_attribute(:qar_fails, @qar_fails)
        end
      end
    when "worse than"
      if @qar.rating == "better than" || @qar.rating == "as good as"
        if @current_user.qar_fails.nil?
          @current_user.update_attribute(:qar_fails, 1)
        else
          @qar_fails = @current_user.qar_fails + 1
          @current_user.update_attribute(:qar_fails, @qar_fails)
        end
      end
    end

    @raters = @qar.raters.split(",").map(&:to_i)
    @raters << @rater_id
    @raters = @raters.join(",")
    @qar.update_attribute(:raters, @raters)

    @total_rated = @current_user.total_rated + 1
    @current_user.update_attribute(:total_rated, @total_rated)

    if @current_user.qar_fails == 2
      mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
      mb_obj = Mailgun::MessageBuilder.new()

      mb_obj.set_from_address("do-not-reply@parrot.com", {"first"=>"Parrot 66", "last" => ""});
      mb_obj.add_recipient(:to, @current_user.email, {"first"=> @current_user.f_name.to_s, "last" => @current_user.l_name.to_s});
      mb_obj.set_subject("P66 | Warning ! ! !");

      result_msg = "<p>Dear "+@current_user.f_name.to_s+" "+@current_user.l_name.to_s+"</p>"
      result_msg += "<p>Be Careful, you have reached the maximum times of QA fails.<br />"
      result_msg += "you will lose ability to rate responses if the QA failed again.</p>"
      result_msg += "<p></p>"
      result_msg += "<p>Best regards,</p>"

      mb_obj.set_html_body(result_msg);
      mg_client.send_message("parrot66.com", mb_obj)
    elsif @current_user.qar_fails == 3
      mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
      mb_obj = Mailgun::MessageBuilder.new()

      mb_obj.set_from_address("do-not-reply@parrot.com", {"first"=>"Parrot 66", "last" => ""});
      mb_obj.add_recipient(:to, @current_user.email, {"first"=> @current_user.f_name.to_s, "last" => @current_user.l_name.to_s});
      mb_obj.set_subject("P66 | Warning ! ! !");

      result_msg = "<p>Dear "+@current_user.f_name.to_s+" "+@current_user.l_name.to_s+"</p>"
      result_msg += "<p>You have exceeded the maximum times of QA fails.<br />"
      result_msg += "you can't rate responses no longer.</p>"
      result_msg += "<p></p>"
      result_msg += "<p>Best regards,</p>"

      mb_obj.set_html_body(result_msg);
      mg_client.send_message("parrot66.com", mb_obj)

      @current_user.update_attribute(:is_rater, 0)
    end

    redirect_to '/rater_available', notice: "Rating Success!"

  end

  # GET /qars/new
  def new
    if !@current_user.admin?
      return redirect_to '/home'
    end

    @qar = Qar.new

    # prompt select
    @prompts = Prompt.all

    @all_prompt_hash = {}
    @all_lang_prompt_hash = {}
    @all_industry_prompt_hash = {}
    @all_level_prompt_hash = {}

    if @prompts.count.to_i > 0
        @prompts.each do |prompt|
            @all_prompt_hash[prompt["id"]] = prompt

            if @all_lang_prompt_hash[prompt["language"]].nil?
                @all_lang_prompt_hash[prompt["language"]] = []
            end

            if @all_industry_prompt_hash[prompt["industry"]].nil?
                @all_industry_prompt_hash[prompt["industry"]] = []
            end

            if @all_level_prompt_hash[prompt["level"]].nil?
                @all_level_prompt_hash[prompt["level"]] = []
            end

            @all_lang_prompt_hash[prompt["language"]] << prompt["id"]
            @all_industry_prompt_hash[prompt["industry"]] << prompt["id"]
            @all_level_prompt_hash[prompt["level"]] << prompt['id']
        end

        # data = { :message => @all_prompt_hash, :status => "false"}
        # return render :json => data, :status => :ok
    end


    # bmr select
    @bmrs = Response.where(:response_type => 1)

    @all_bmr_hash = {}
    @all_lang_bmr_hash = {}
    @all_industry_bmr_hash = {}
    @all_level_bmr_hash = {}

    if @bmrs.count.to_i > 0
        @bmrs.each do |bmr|
            @all_bmr_hash[bmr["id"]] = bmr

            if @all_lang_bmr_hash[bmr["language"]].nil?
                @all_lang_bmr_hash[bmr["language"]] = []
            end

            if @all_industry_bmr_hash[bmr["industry"]].nil?
                @all_industry_bmr_hash[bmr["industry"]] = []
            end


            if @all_level_bmr_hash[bmr["level"]].nil?
                @all_level_bmr_hash[bmr["level"]] = []
            end

            @all_lang_bmr_hash[bmr["language"]] << bmr["id"]
            @all_industry_bmr_hash[bmr["industry"]] << bmr["id"]
            @all_level_bmr_hash[bmr["level"]] << bmr["id"]
        end
    end


    # response select
    @responses = Response.where(:response_type => 0)

    @all_response_hash = {}
    @all_lang_response_hash = {}
    @all_industry_response_hash = {}
    @all_level_response_hash = {}

    if @responses.count.to_i > 0
        @responses.each do |response|
            @all_response_hash[response["id"]] = response

            if @all_lang_response_hash[response["language"]].nil?
                @all_lang_response_hash[response["language"]] = []
            end

            if @all_industry_response_hash[response["industry"]].nil?
                @all_industry_response_hash[response["industry"]] = []
            end


            if @all_level_response_hash[response["level"]].nil?
                @all_level_response_hash[response["level"]] = []
            end

            @all_lang_response_hash[response["language"]] << response["id"]
            @all_industry_response_hash[response["industry"]] << response["id"]
            @all_level_response_hash[response["level"]] << response["id"]
        end
    end

  end

  # GET /qars/1/edit
  def edit
    @qar = Qar.find(params[:id])
  end

  # POST /qars
  # POST /qars.json
  def create
    @qar = Qar.new(qar_params)

    if @qar.title.to_s == "" || @qar.language.to_s == "" || @qar.industry.to_s == "" || @qar.level.to_s == "" || @qar.prompt.to_s == "" || @qar.bmr.to_s == "" || @qar.response.to_s == "" || @qar.rating.to_s == ""
      return redirect_to new_qar_path, notice: "Please fill in all required fields"
    end

    respond_to do |format|
      if @qar.save
        format.html { redirect_to qars_path, notice: 'Qar was successfully created.' }
        format.json { render :show, status: :created, location: @qar }
      else
        format.html { render :new }
        format.json { render json: @qar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /qars/1
  # PATCH/PUT /qars/1.json
  def update
    @qar = Qar.find(params[:id])

    respond_to do |format|
      if @qar.update(qar_params)
        format.html { redirect_to qars_path, notice: 'Qar was successfully updated.' }
        format.json { render :show, status: :ok, location: @qar }
      else
        format.html { render :edit }
        format.json { render json: @qar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /qars/1
  # DELETE /qars/1.json
  def destroy
    @qar.destroy
    respond_to do |format|
      format.html { redirect_to qars_url, notice: 'Qar was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_qar
      @qar = Qar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def qar_params
      params.fetch(:qar, {})

      params.require(:qar).permit(:title, :language, :industry, :level, :prompt, :bmr, :response, :rating)
    end
end
