class ContactsController < ApplicationController

  def new
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]

    @contact = Contact.new
  end

  def create
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]
    
    @contact = Contact.new(params[:contact])

    if @contact.name == "" || @contact.email == "" || @contact.subject == "" || @contact.message == ""
        return redirect_to new_contact_path, notice: "Please make sure you input all require field."
    end
    # @contact.request = request
    # if @contact.deliver
    #   flash.now[:notice] = 'Thank you for your message. We will contact you soon!'
    # else
    #   flash.now[:error] = 'Cannot send message.'
    #   render :new
    # end
    mg_client = Mailgun::Client.new("key-e55d9349c3a7816034d2dab0ae5b8808")
    mb_obj = Mailgun::MessageBuilder.new()

    mb_obj.set_from_address(@contact.email, {"first"=>@contact.name, "last" => ""});
    mb_obj.add_recipient(:to, "lzhuang@bmgww.com", {"first"=> "Leon", "last" => "Huang"});
    mb_obj.set_subject(@contact.subject);

    result_msg = @contact.message


    mb_obj.set_html_body(result_msg);
    mg_client.send_message("parrot66.com", mb_obj)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.fetch(:contact, {})

      params.require(:contact).permit(:name, :email, :subject, :message)
    end

end
