class HomesController < ApplicationController
  before_action :anti_authuser, except: [:find_page] #to show howitworks page when user clicks the link on certification page

  def find_page
    @pages = Page.all
    @page = Page.find_by_slug(params[:slug])

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]

    # data = {:message => @page, :status => "false"}
    # return render :json => data, :status => :ok

    if @page.nil?
      return redirect_to notfound_path
    end

    case @page.page_type
    when "home"
      render template: "homes/index"

    when "howitworks"
      @tests = Test.where(:status => 1).paginate(:page => params[:page], :per_page => 5)
      render template: "homes/howitworks"

    when "aboutthetests"
      @tests = Test.where(:status => 1).paginate(:page => params[:page], :per_page => 6)
      @practice_test = Test.where(:is_practice => true).order(:updated_at => :desc).first # to practice test on aboutthetests page by Johann
      render template: "homes/aboutthetests"

    when "aboutparrot"
      render template: "homes/aboutparrot"

    when "parrotforbusiness"
      render template: "homes/parrotforbusiness"

    when "howtobecomeaparrotrater"
      @tests = Test.where(:status => 1).paginate(:page => params[:page], :per_page => 5)
      render template: "homes/howtobecomeaparrotrater"

    when "faq"
      render template: "homes/faq"

    when "privatepolicy"
      render template: "homes/privatepolicy"

    end

  end

  def index
    @pages = Page.all
    @page = Page.find_by_page_type("home")
    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]
    #  data = {:message => @page, :status => "false"}
    # return render :json => data, :status => :ok


  end

  def test_introduction
    @aboutthetests = Page.find_by_page_type("aboutthetests")


    @test = Test.find(params[:id])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def search_tests
    @language = params[:language]
    @industry = params[:industry]
     #  data = {:message => @industry, :status => "false"}
     # return render :json => data, :status => :ok

    if @language != "" && @industry == ""
      @tests = Test.where(:status => 1, :language => @language).order("created_at DESC").paginate(:page => params[:page], :per_page => 6)
      render :partial => 'tests'
    elsif @industry != "" && @language == ""
      @tests = Test.where(:status => 1, :industry => @industry).order("created_at DESC").paginate(:page => params[:page], :per_page => 6)
      render :partial => 'tests'
    elsif @language != "" && @industry != ""
      @tests = Test.where(:status => 1, :industry => @industry, :language => @language).order("created_at DESC").paginate(:page => params[:page], :per_page => 6)
      #  data = {:message => @tests, :status => "false"}
      # return render :json => data, :status => :ok
      render :partial => 'tests'
    end
  end

  def cart_number_reload
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]
    
    render :partial => 'header'
  end

  def thankyou
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]

    if session[:sign_up_email] == nil
        redirect_to "/"
    end
  end


  def active_link_fail
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]
  end


  def active_link_done
    @pages = Page.all

    @howitworks = @pages[1]
    @aboutthetests = @pages[2]
    @aboutparrot = @pages[3]
    @parrotforbusiness = @pages[4]
    @howtobecomeaparrotrater = @pages[5]
    @footer = @pages[8]
  end

end
