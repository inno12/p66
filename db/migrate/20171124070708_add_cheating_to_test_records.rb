class AddCheatingToTestRecords < ActiveRecord::Migration[5.1]
  def change
    add_column :test_records, :cheating, :integer, default: 0
  end
end
