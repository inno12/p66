class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string  :title
      t.string  :slug
      t.text  :body, :limit => 4294967295
      t.text  :section1, :limit => 4294967295
      t.text  :section2, :limit => 4294967295
      t.text  :section3, :limit => 4294967295
      t.text  :section4, :limit => 4294967295
      t.text  :section5, :limit => 4294967295
      t.text  :section6, :limit => 4294967295
      t.text  :section7, :limit => 4294967295
      t.text  :section8, :limit => 4294967295
      t.text  :section9, :limit => 4294967295
      t.string  :image1
      t.string  :image2
      t.string  :image3
      t.string  :image4
      t.string  :page_type

      t.timestamps
    end
  end
end
