class ChangePageDataSize < ActiveRecord::Migration[5.1]
  def change
    change_column :pages, :body, :text, :limit => 4294967295
    change_column :pages, :section1, :text, :limit => 4294967295
    change_column :pages, :section2, :text, :limit => 4294967295
    change_column :pages, :section3, :text, :limit => 4294967295
    change_column :pages, :section4, :text, :limit => 4294967295
    change_column :pages, :section5, :text, :limit => 4294967295
    change_column :pages, :section6, :text, :limit => 4294967295

  end
end
