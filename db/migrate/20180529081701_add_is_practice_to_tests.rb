class AddIsPracticeToTests < ActiveRecord::Migration[5.1]
  def change
    add_column :tests, :is_practice, :boolean, :default => false
  end
end
