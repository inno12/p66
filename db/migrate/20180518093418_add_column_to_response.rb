class AddColumnToResponse < ActiveRecord::Migration[5.1]
  def change
    add_column :responses, :cheating_detected, :boolean, :default => false
  end
end
