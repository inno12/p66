class AddTimeLimitToPrompts < ActiveRecord::Migration[5.1]
  def change
    add_column :prompts, :time_limit, :integer
  end
end
