class AddTotalQarsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :qar_fails, :integer
  end
end
