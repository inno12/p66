# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
line_items = LineItem.create([
    { description: 'Subscription April 2016', price: 000.0 } ])
Invoice.create(
    client: 'Sample ABC', 
    total: 000.0, 
    line_items: line_items, 
    date: Date.new(2018, 4, 29))
